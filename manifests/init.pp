class logstash (
    $repository_manage,
    $repository_ensure,
    $repository_type,
    $repository_key_source,
    $repository_version,
    $package_ensure,
    $package_name,
    $package_manage,
    $java_package_ensure,
    $java_package_name,
    $java_package_manage,
    $config_manage,
    $service_enable,
    $service_ensure,
    $service_manage,
    $service_name,
    $config_manage,
  ) {

  anchor { 'logstash::begin': } ->
  class { '::logstash::repository': } ->
  class { '::logstash::install': } ->
  class { '::logstash::config': } ~>
  class { '::logstash::service': } ->
  anchor { 'logstash::end': }
}
