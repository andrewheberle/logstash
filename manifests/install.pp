class logstash::install inherits logstash {
  if $logstash::package_manage {
    if $logstash::java_package_manage {
      package { $logstash::java_package_name:
        ensure => $logstash::java_package_ensure,
        before => Package[$logstash::package_name],
      }
    }

    package { $logstash::package_name:
      ensure => $logstash::package_ensure,
    }
  }
}