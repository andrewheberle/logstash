class logstash::repository inherits logstash {
  if $logstash::repository_manage {
    case $logstash::repository_type {
      'yum': {
        yumrepo { 'logstash':
          ensure   => $logstash::repository_ensure,
          name     => "logstash-${logstash::repository_version}",
          descr    => "Logstash repository for ${logstash::repository_version}.x packages",
          baseurl  => "http://packages.elastic.co/logstash/${logstash::repository_version}/centos",
          gpgcheck => true,
          gpgkey   => $logstash::repository_key_source,
          enabled  => true,
        }
      }
      'apt': {
        include apt
        apt::source { 'logstash':
          comment  => "Logstash repository for ${logstash::repository_version}.x packages",
          location => "http://packages.elastic.co/logstash/${logstash::repository_version}/debian",
          release  => 'stable',
          repos    => 'main',
          key      => {
            'id'     => '46095ACC8548582C1A2699A9D27D666CD88E42B4',
            'server' => 'pgp.mit.edu',
            'source' => $logstash::repository_key_source,
          },
          include  => {
            'deb' => true,
            'src' => false,
          },
        }
      }
      default: {
        fail("Module ${module_name} is only supported with apt or yum based distros")
      }
    }
  }
}