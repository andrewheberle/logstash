class logstash::service inherits logstash {
  if $logstash::service_manage {
    service {$logstash::service_name:
      ensure => $logstash::service_ensure,
      enable => $logstash::service_enable,
    }
  }
}